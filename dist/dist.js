(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["flaap"] = factory();
	else
		root["flaap"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.bindings = undefined;

var _observer = __webpack_require__(4);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } } /*
                                                                                                                                                             ___ _           _ _                 
                                                                                                                                                            / __(_)_ __   __| (_)_ __   __ _ ___ 
                                                                                                                                                           /__\// | '_ \ / _` | | '_ \ / _` / __|
                                                                                                                                                          / \/  \ | | | | (_| | | | | | (_| \__ \
                                                                                                                                                          \_____/_|_| |_|\__,_|_|_| |_|\__, |___/
                                                                                                                                                                                       |___/     
                                                                                                                                                          */

// :: AddBindings :: this.will add all event listsener bindings.
// will keep a list of bindings based on view/sub-view

var actions = {
  click: 'flaap-click'
};

var bindingCount = 0;

var Bindings = function Bindings() {
  _classCallCheck(this, Bindings);

  this.add = {
    click: function click() {
      return addEventCallbacks('click', actions.click);
    },
    data: function data(text) {
      if (text) {
        return renderedText(text);
      }
    },
    mouseenter: function mouseenter() {},
    mouseleave: function mouseleave() {}
  };
};

var addEventCallbacks = function addEventCallbacks(action, tag) {
  var elements = document.querySelectorAll('[' + tag + ']');

  elements.forEach(function (el) {
    var bindingValue = el.getAttribute(tag);
    el.removeAttribute(tag);

    if (isMethodCall(bindingValue)) {
      var methodName = getMethodName(bindingValue);
      var args = getArguments(bindingValue);
      console.log(' ::>> args = ', args);

      el.addEventListener(action, function () {
        if (window.viewModel && window.viewModel[methodName] && isInRoute(el)) {
          window.viewModel[methodName].apply(null, args);
        } else if (!isInRoute(el)) {
          window[methodName] && window[methodName].apply(null, args);
        }
      });
    }
  });
};

var renderedText = function renderedText(text) {
  var replaceExpression = /\${|}/g;
  var matches = findBindables(text);

  if (!matches || !matches.length) return '';

  var bindableExpression = /(\${.*})/g;
  matches.forEach(function (bind) {
    bindingCount++;
    var instance = bindingCount;

    var variableName = bind.replace(replaceExpression, '');
    var value = window.viewModel[variableName] || '';
    var mappedValue = getBoundComments(instance, variableName, value);
    text = text.replace(bind, mappedValue);

    if (window.viewModel[variableName]) {
      _observer.observe.addStringHandler(variableName, function (newValue) {
        var oldValue = getBoundComments(instance, variableName, value);
        var newMappedValue = oldValue.replace(value, newValue);
        var router = document.querySelector('flaap-route');
        router.innerHTML = router.innerHTML.replace(oldValue, newMappedValue);
        value = newValue;
      });
    }
  });

  var modifiedText = text;
  return modifiedText;
};

var findBindables = function findBindables(text) {
  var bindableVariableExpression = /\${(.*)}/g;
  console.log(' ::>> value = ', text.match(bindableVariableExpression));
  return text.match(bindableVariableExpression);
};

var isInRoute = function isInRoute(node) {
  return document.querySelector('flaap-route').contains(node);
};

var isMethodCall = function isMethodCall(bindingValue) {
  return bindingValue.includes('(') && bindingValue.includes(')');
};

var getMethodName = function getMethodName(bindingValue) {
  return bindingValue.split('(')[0];
};

var getArguments = function getArguments(bindingValue) {
  var args = bindingValue.split('(')[1].replace(')', '');
  if (args.includes("'")) {
    args = args.replace(/'/g, '');
  }
  return args.split(',').map(function (item) {
    return item.trim();
  });
};

var getBoundComments = function getBoundComments(instance, variableName, value) {
  return getBindStart(instance, variableName) + value + getBindEnd(instance, variableName);
};

var getBindStart = function getBindStart(_bindingCount, variableName) {
  return '<!--flaap-target-' + _bindingCount + '-->';
};

var getBindEnd = function getBindEnd(_bindingCount, variableName) {
  return '<!--flaap-target-' + _bindingCount + '/-->';
};

var bindings = new Bindings();
exports.bindings = bindings;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _main = __webpack_require__(3);

var __flaapworks__ = function configure(flaapworks) {
  return flaapworks.configure //
  .routes([{ route: ['', 'one'], module: 'test/views/one/one', name: 'one' }, //
  { route: 'two', module: 'test/views/two/two', name: 'two' }]);
}(_main.flaapworks); /*
                        __                           _                                    
                       /__\_  ____ _ _ __ ___  _ __ | | ___   /\ /\  ___  __ _  __ _  ___ 
                      /_\ \ \/ / _` | '_ ` _ \| '_ \| |/ _ \ / / \ \/ __|/ _` |/ _` |/ _ \
                     //__  >  < (_| | | | | | | |_) | |  __/ \ \_/ /\__ \ (_| | (_| |  __/
                     \__/ /_/\_\__,_|_| |_| |_| .__/|_|\___|  \___/ |___/\__,_|\__, |\___|
                                              |_|                              |___/      
                     */

window.routeTo = function (state, test) {
  console.log(' ::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> data >> ', { state: state, test: test });
  __flaapworks__.router.navigate(state);
};

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/*
   __                 _     ___           
  /__\_   _____ _ __ | |_  / __\_   _ ___ 
 /_\ \ \ / / _ \ '_ \| __|/__\// | | / __|
//__  \ V /  __/ | | | |_/ \/  \ |_| \__ \
\__/   \_/ \___|_| |_|\__\_____/\__,_|___/
                                          
*/

var EventBus = {
  subscriptions: {},

  init: function init(view) {
    return {
      view: view,
      publish: function publish(event, data) {
        console.log('%c ::>> Publishing Event ' + event, 'color:white;background:lightblue;');
        if (EventBus.subscriptions[event]) {
          for (var key in EventBus.subscriptions[event]) {
            if (EventBus.subscriptions[event].hasOwnProperty(key)) {
              EventBus.subscriptions[event][key](event, data);
            }
          }
        }
      },
      subscribe: function subscribe(event, callback) {
        console.log('%c ::>> Subscribing to Event ' + event + ' in ' + view, 'color:white;background:lightgreen;');
        EventBus.subscriptions[event] = EventBus.subscriptions[event] || {};
        EventBus.subscriptions[event][this.view] = EventBus.subscriptions[event][this.view] || callback;
      },
      subscribeOnce: function subscribeOnce(event, callback) {
        var _this = this;

        this.subscribe(event, function (data) {
          _this.unsubscribe(event);
          callback(data);
        });
      },
      unsubscribe: function unsubscribe(event) {
        if (EventBus.subscriptions[event] && EventBus.subscriptions[event][this.view]) {
          delete EventBus.subscriptions[event][this.view];

          if (!Object.keys(EventBus.subscriptions[event]).length) {
            delete EventBus.subscriptions[event];
          }
        }
        console.log('%c ::>> removed subscription ', 'color:white;background:red;');
        console.log(' ::>> EventBus.subscriptions = ', EventBus.subscriptions);
      }
    };
  }
};

exports.EventBus = EventBus;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.flaapworks = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ___ _                                     _        
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       / __\ | __ _  __ _ _ ____      _____  _ __| | _____ 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      / _\ | |/ _` |/ _` | '_ \ \ /\ / / _ \| '__| |/ / __|
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     / /   | | (_| | (_| | |_) \ V  V / (_) | |  |   <\__ \
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     \/    |_|\__,_|\__,_| .__/ \_/\_/ \___/|_|  |_|\_\___/
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |_|                               
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     */


var _router = __webpack_require__(5);

var _events = __webpack_require__(2);

var _bindings = __webpack_require__(0);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FlaapWorks = function () {
  function FlaapWorks() {
    _classCallCheck(this, FlaapWorks);

    console.log(' initialising app... ');
    this.initialiseSubscriptions();

    this.configure = {
      eventBus: this.eventBus,
      components: this.setupComponents,
      routes: this.setupRoutes,
      initialiseSubscriptions: this.initialiseSubscriptions
    };
    return this;
  }

  _createClass(FlaapWorks, [{
    key: 'setupComponents',
    value: function setupComponents(data) {
      console.log(' ::>> components => ', data);
      if (Array.isArray(data) && data.length) {
        // initialise components
      }
      return this;
    }
  }, {
    key: 'setupRoutes',
    value: function setupRoutes(data) {
      console.log(' ::>> routes => ', data);
      if (Array.isArray(data) && data.length) {
        // initialise routes
        this.router = new _router.Router(data, this.eventBus);
      } else {
        console.log(' Unable to setup routes as no routes have been specified. ');
      }
      return this;
    }
  }, {
    key: 'initialiseSubscriptions',
    value: function initialiseSubscriptions() {
      this.eventBus = _events.EventBus.init('main');
      _bindings.bindings.add.click();

      this.eventBus.subscribe('flaap:route:activated', function (data) {
        console.log('%c ::>> received event ', 'color:white;background:orange;');
        console.log(' :: flaap:route:activated :: ', data);
      });

      this.eventBus.subscribe('flaap:route:attached', function (data) {
        console.log('%c ::>> received event ', 'color:white;background:orange;');
        console.log(' :: flaap:route:attached :: ', data);

        // add bindings
        _bindings.bindings.add.click();
      });
    }
  }]);

  return FlaapWorks;
}();

var flaapworks = new FlaapWorks();
exports.flaapworks = flaapworks;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/*
   ___ _                                  
  /___\ |__  ___  ___ _ ____   _____ _ __ 
 //  // '_ \/ __|/ _ \ '__\ \ / / _ \ '__|
/ \_//| |_) \__ \  __/ |   \ V /  __/ |   
\___/ |_.__/|___/\___|_|    \_/ \___|_|   
                                          
*/

var observe = {
  addStringHandler: function addStringHandler(variable, callback) {
    window.viewModel[variable] = {
      set: function set(newValue) {
        this[variable] = newValue || this[variable];
        callback(newValue);
      },
      get: function get() {
        return this[variable];
      }
    };
  },
  object: function object(obj, handler) {
    var proxy = {};

    var _loop = function _loop(key) {
      if (obj.hasOwnProperty(key)) {
        proxy['set_' + key] = function (val) {
          this[key] = val;
        };
        proxy['get_' + key] = function (val) {
          return this[key];
        };
      }
    };

    for (var key in obj) {
      _loop(key);
    }
    for (var key in proxy) {
      if (proxy.hasOwnProperty(key)) {
        obj[key] = proxy[key];
      }
    }
    observe.addObjectHandler(obj, handler);
  },
  addObjectHandler: function addObjectHandler(obj, handler) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        (function () {
          var current_setter = obj['set_' + prop];
          var old_val = obj['get_' + prop]();

          obj['set_' + prop] = function (val) {
            current_setter.apply(obj, [old_val, val]);
            handler(val);
          };
        })();
      }
    }
  },
  array: function array(arr, handler) {
    if (arr.constructor !== Array) return;
    var proxy = {};

    proxy['set_list'] = function (val) {
      this.list = val;
    };
    proxy['get_list'] = function (val) {
      return this.list;
    };

    proxy['set_length'] = function (val) {
      this.length = val;
    };
    proxy['get_length'] = function (val) {
      return this.length;
    };
    for (var key in proxy) {
      if (proxy.hasOwnProperty(key)) {
        arr[key] = proxy[key];
      }
    }
    observe.addArrayHandler(arr, handler);
  },
  addArrayHandler: function addArrayHandler(arr, handler) {
    var current_setter = arr['set_length'];
    var old_val = arr['get_length']();

    arr['set_length'] = function (val) {
      current_setter.apply(arr, [old_val, val]);
      handler(arr.list);
    };
  }
};
exports.observe = observe;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Router = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /*
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        __             _            
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       /__\ ___  _   _| |_ ___ _ __ 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      / \/// _ \| | | | __/ _ \ '__|
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     / _  \ (_) | |_| | ||  __/ |   
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     \/ \_/\___/ \__,_|\__\___|_|   
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     */


var _bindings = __webpack_require__(0);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ROUTER_VIEW = 'flaap-route';

var Router = exports.Router = function () {
  function Router(config, eventBus) {
    var _this = this;

    _classCallCheck(this, Router);

    this.config = config;
    this.eventBus = eventBus;

    this.init();
    window.onpopstate = function (event) {
      return _this.init(true);
    };
  }

  _createClass(Router, [{
    key: 'init',
    value: function init(ignorePushState) {
      this.el = document.querySelector(ROUTER_VIEW);

      if (!this.el) {
        return;
      }
      activateRoute(this.el, findModule(this.config), ignorePushState, this.eventBus);
    }
  }, {
    key: 'navigate',
    value: function navigate(state) {
      console.log(' ::>> navigate >>>> ', state);
      if (isCurrentState(state, getCurrentRoute())) {
        return;
      }
      activateRoute(this.el, findModule(this.config, state), null, this.eventBus);
    }
  }]);

  return Router;
}();

var findModule = function findModule(config, _state) {
  var state = getCurrentRoute();
  if (isCurrentState(state, _state)) {
    return;
  }
  state = _state || state;
  if (noPathFound(state)) {
    return config[0];
  }
  return findAndSetState(state, config);
};

var getCurrentRoute = function getCurrentRoute() {
  var href = location.href;
  return href.substring(href.lastIndexOf('#') + 1, href.length);
};

var isCurrentState = function isCurrentState(state, _state) {
  return state === _state;
};

var noPathFound = function noPathFound(state) {
  return !state || state === '';
};

var findAndSetState = function findAndSetState(state, config) {
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = config[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var item = _step.value;

      if (item.name === state) {
        return item;
      }
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  return config[0];
};

var activateRoute = function activateRoute(el, state, ignorePushState, eventBus) {
  if (!state) {
    return;
  }

  deactivateState(eventBus);
  activateState(eventBus, state, el);

  if (!ignorePushState) {
    window.history.pushState({}, state.name, '#' + state.name);
  }
};

var load = function load(_module) {
  return {
    into: function into(el) {
      return new Promise(function (resolve) {
        getResource(_module).then(function (data) {
          resolve(data);
        });
      });
    }
  };
};

var deactivateState = function deactivateState(eventBus) {
  if (window.viewModel) {
    window.viewModel.deactivate && window.viewModel.deactivate();
    delete window.viewModel;
    eventBus.publish('flaap:route:deactivated', {});
  }
};

var activateState = function activateState(eventBus, state, el) {
  load(state.module + '.js').into(el).then(function (text) {
    var viewModel = '';
    try {
      text = 'return ' + text;
      viewModel = new Function(text)();
      window.viewModel = new viewModel();
      window.viewModel.activate && window.viewModel.activate();
      eventBus.publish('flaap:route:activated', {});
    } catch (e) {
      console.error(' ::>> unable to get viewModel ', e);
    }

    load(state.module + '.html').into(el).then(function (view) {
      view = _bindings.bindings.add.data(view);
      el.innerHTML = view;
      if (!el.className.includes(' _routed_')) {
        el.className += ' _routed_';
      }
      window.viewModel.attached && window.viewModel.attached();
      eventBus.publish('flaap:route:attached', {});
    });
  });
};

var getResource = function getResource(source) {
  return new Promise(function (resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', source, true);

    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status >= 200 && xhr.status < 400) {
          resolve(xhr.response);
        } else {
          console.error('fail > error = ', xhr);
          reject(xhr.error);
        }
      }
    };
    xhr.send();
  });
};

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ })
/******/ ]);
});