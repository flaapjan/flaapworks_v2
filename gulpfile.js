const gulp = require('gulp');
const gutil = require('gulp-util');
const webpack = require('webpack');
const config = require('./config');
const del = require('del');
const WebpackDevServer = require('webpack-dev-server');
const vinylPaths = require('vinyl-paths');

gulp.task('default', ['build', 'run']);

gulp.task('clean', () => {
  return gulp.src('dist').pipe(vinylPaths(del));
});

gulp.task('build', ['clean'], callback => {
  webpack(config, (err, stats) => {
    if (err) throw new gutil.PluginError('webpack', err);

    gutil.log(
      '[build]',
      stats.toString({
        colors: true
      })
    );

    callback();
  });
});

gulp.task('run', callback => {
  var compiler = webpack(config);
  var options = { publicPath: config.output.publicPath };

  new WebpackDevServer(compiler, options).listen(8080, 'localhost', err => {
    if (err) throw new gutil.PluginError('webpack-dev-server', err);

    gutil.log('[run]', 'Server started: http://localhost:8080/');
  });
});
