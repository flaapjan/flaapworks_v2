/*
   ___ _                                  
  /___\ |__  ___  ___ _ ____   _____ _ __ 
 //  // '_ \/ __|/ _ \ '__\ \ / / _ \ '__|
/ \_//| |_) \__ \  __/ |   \ V /  __/ |   
\___/ |_.__/|___/\___|_|    \_/ \___|_|   
                                          
*/

const observe = {
  addStringHandler: (variable, handler) => {
    window.viewModel[variable] = {
      set: function(newValue) {
        const oldValue = this[variable];
        handler(newValue, oldValue);
        this[variable] = newValue || this[variable];
      },
      get: function() {
        return this[variable];
      }
    };
  },
  object: function(obj, handler) {
    let proxy = {};

    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        proxy['set_' + key] = function(val) {
          this[key] = val;
        };
        proxy['get_' + key] = function(val) {
          return this[key];
        };
      }
    }
    for (let key in proxy) {
      if (proxy.hasOwnProperty(key)) {
        obj[key] = proxy[key];
      }
    }
    observe.addObjectHandler(obj, handler);
  },
  addObjectHandler: function(obj, handler) {
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        let current_setter = obj['set_' + prop];
        let oldValue = obj['get_' + prop]();

        obj['set_' + prop] = function(newValue) {
          current_setter.apply(obj, [oldValue, newValue]);
          handler(newValue, oldValue);
        };
      }
    }
  },
  array: function(arr, handler) {
    if (arr.constructor !== Array) return;
    let proxy = {};

    proxy['set_list'] = function(val) {
      this.list = val;
    };
    proxy['get_list'] = function(val) {
      return this.list;
    };

    proxy['set_length'] = function(val) {
      this.length = val;
    };
    proxy['get_length'] = function(val) {
      return this.length;
    };
    for (let key in proxy) {
      if (proxy.hasOwnProperty(key)) {
        arr[key] = proxy[key];
      }
    }
    observe.addArrayHandler(arr, handler);
  },
  addArrayHandler: function(arr, handler) {
    let current_setter = arr['set_length'];
    let oldValue = arr['get_length']();

    arr['set_length'] = function(val) {
      current_setter.apply(arr, [oldValue, val]);
      handler(arr.list, oldValue);
    };
  }
};

//todo: cater for object changes inside of the array

export { observe };
