/*
   ___ _                                     _        
  / __\ | __ _  __ _ _ ____      _____  _ __| | _____ 
 / _\ | |/ _` |/ _` | '_ \ \ /\ / / _ \| '__| |/ / __|
/ /   | | (_| | (_| | |_) \ V  V / (_) | |  |   <\__ \
\/    |_|\__,_|\__,_| .__/ \_/\_/ \___/|_|  |_|\_\___/
                    |_|                               
*/
import { Router } from './router';
import { EventBus } from './events';
import { bindings } from './bindings';
import { observe } from './observer';

class FlaapWorks {
  constructor() {
    console.log(' initialising app... ');
    this.initialiseSubscriptions();

    this.configure = {
      eventBus: this.eventBus,
      components: this.setupComponents,
      routes: this.setupRoutes,
      initialiseSubscriptions: this.initialiseSubscriptions
    };
    return this;
  }

  setupComponents(data) {
    console.log(' ::>> components => ', data);
    if (Array.isArray(data) && data.length) {
      // initialise components
    }
    return this;
  }

  setupRoutes(data) {
    console.log(' ::>> routes => ', data);
    if (Array.isArray(data) && data.length) {
      // initialise routes
      this.router = new Router(data, this.eventBus);
    } else {
      console.log(' Unable to setup routes as no routes have been specified. ');
    }
    return this;
  }

  initialiseSubscriptions() {
    this.eventBus = EventBus.init('main');
    bindings.add.click();

    this.eventBus.subscribe('flaap:route:activated', data => {
      console.log('%c ::>> received event ', 'color:white;background:orange;');
      console.log(' :: flaap:route:activated :: ', data);
    });

    this.eventBus.subscribe('flaap:route:attached', data => {
      console.log('%c ::>> received event ', 'color:white;background:orange;');
      console.log(' :: flaap:route:attached :: ', data);

      // add bindings
      bindings.add.click();
    });
  }
}

const flaapworks = new FlaapWorks();
export { flaapworks, observe, EventBus };
