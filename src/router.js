/*
   __             _            
  /__\ ___  _   _| |_ ___ _ __ 
 / \/// _ \| | | | __/ _ \ '__|
/ _  \ (_) | |_| | ||  __/ |   
\/ \_/\___/ \__,_|\__\___|_|   
                               
*/
import { bindings } from './bindings';

const ROUTER_VIEW = 'flaap-route';

export class Router {
  constructor(config, eventBus) {
    this.config = config;
    this.eventBus = eventBus;

    this.init();
    window.onpopstate = event => this.init(true);
  }

  init(ignorePushState) {
    this.el = document.querySelector(ROUTER_VIEW);

    if (!this.el) {
      return;
    }
    activateRoute(this.el, findModule(this.config), ignorePushState, this.eventBus);
  }

  navigate(state) {
    console.log(' ::>> navigate >>>> ', state);
    if (isCurrentState(state, getCurrentRoute())) {
      return;
    }
    activateRoute(this.el, findModule(this.config, state), null, this.eventBus);
  }
}

const findModule = (config, _state) => {
  let state = getCurrentRoute();
  if (isCurrentState(state, _state)) {
    return;
  }
  state = _state || state;
  if (noPathFound(state)) {
    return config[0];
  }
  return findAndSetState(state, config);
};

const getCurrentRoute = () => {
  const href = location.href;
  return href.substring(href.lastIndexOf('#') + 1, href.length);
};

const isCurrentState = (state, _state) => {
  return state === _state;
};

const noPathFound = state => {
  return !state || state === '';
};

const findAndSetState = (state, config) => {
  for (let item of config) {
    if (item.name === state) {
      return item;
    }
  }
  return config[0];
};

const activateRoute = (el, state, ignorePushState, eventBus) => {
  if (!state) {
    return;
  }

  deactivateState(eventBus);
  activateState(eventBus, state, el);

  if (!ignorePushState) {
    window.history.pushState({}, state.name, '#' + state.name);
  }
};

const load = function(_module) {
  return {
    into: el => {
      return new Promise(resolve => {
        getResource(_module).then(data => {
          resolve(data);
        });
      });
    }
  };
};

const deactivateState = eventBus => {
  if (window.viewModel) {
    window.viewModel.deactivate && window.viewModel.deactivate();
    delete window.viewModel;
    eventBus.publish('flaap:route:deactivated', {});
  }
};

const activateState = (eventBus, state, el) => {
  load(state.module + '.js')
    .into(el)
    .then(text => {
      let viewModel = '';
      try {
        text = 'return ' + text;
        viewModel = new Function(text)();
        window.viewModel = new viewModel();
        window.viewModel.activate && window.viewModel.activate();
        eventBus.publish('flaap:route:activated', {});
      } catch (e) {
        console.error(' ::>> unable to get viewModel ', e);
      }

      load(state.module + '.html')
        .into(el)
        .then(view => {
          view = bindings.add.data(view);
          el.innerHTML = view;
          if (!el.className.includes(' _routed_')) {
            el.className += ' _routed_';
          }
          window.viewModel.attached && window.viewModel.attached();
          eventBus.publish('flaap:route:attached', {});
        });
    });
};

const getResource = source => {
  return new Promise((resolve, reject) => {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', source, true);

    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status >= 200 && xhr.status < 400) {
          resolve(xhr.response);
        } else {
          console.error('fail > error = ', xhr);
          reject(xhr.error);
        }
      }
    };
    xhr.send();
  });
};
