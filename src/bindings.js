/*
   ___ _           _ _                 
  / __(_)_ __   __| (_)_ __   __ _ ___ 
 /__\// | '_ \ / _` | | '_ \ / _` / __|
/ \/  \ | | | | (_| | | | | | (_| \__ \
\_____/_|_| |_|\__,_|_|_| |_|\__, |___/
                             |___/     
*/

// :: AddBindings :: this.will add all event listsener bindings.
// will keep a list of bindings based on view/sub-view

import { observe } from './observer';

const actions = {
  click: 'flaap-click'
};

let bindingCount = 0;

class Bindings {
  add = {
    click: () => addEventCallbacks('click', actions.click),
    data: text => {
      if (text) {
        return template(text);
      }
    },
    mouseenter: () => {},
    mouseleave: () => {}
  };
}

const addEventCallbacks = (action, tag) => {
  let elements = document.querySelectorAll('[' + tag + ']');

  elements.forEach(el => {
    let bindingValue = el.getAttribute(tag);
    el.removeAttribute(tag);

    if (isMethodCall(bindingValue)) {
      const methodName = getMethodName(bindingValue);
      const args = getArguments(bindingValue);
      console.log(' ::>> args = ', args);

      el.addEventListener(action, () => {
        if (window.viewModel && window.viewModel[methodName] && isInRoute(el)) {
          window.viewModel[methodName].apply(null, args);
        } else if (!isInRoute(el)) {
          window[methodName] && window[methodName].apply(null, args);
        }
      });
    }
  });
};

const template = text => {
  const replaceExpression = /\${|}/g;
  const matches = findBindables(text);

  if (!matches || !matches.length) return '';

  const bindableExpression = /(\${.*})/g;
  matches.forEach(bind => {
    bindingCount++;
    const instance = bindingCount;

    const variableName = bind.replace(replaceExpression, '');
    let value = window.viewModel[variableName] || '';
    let mappedValue = getBoundComments(instance, variableName, value);
    text = text.replace(bind, mappedValue);

    if (window.viewModel[variableName]) {
      observe.addStringHandler(variableName, newValue => {
        let oldValue = getBoundComments(instance, variableName, value);
        let newMappedValue = oldValue.replace(value, newValue);
        let router = document.querySelector('flaap-route');
        router.innerHTML = router.innerHTML.replace(oldValue, newMappedValue);
        value = newValue;
      });
    }
  });

  let modifiedText = text;
  return modifiedText;
};

const findBindables = text => {
  const bindableVariableExpression = /\${(.*)}/g;
  console.log(' ::>> value = ', text.match(bindableVariableExpression));
  return text.match(bindableVariableExpression);
};

const isInRoute = node => {
  return document.querySelector('flaap-route').contains(node);
};

const isMethodCall = bindingValue => {
  return bindingValue.includes('(') && bindingValue.includes(')');
};

const getMethodName = bindingValue => {
  return bindingValue.split('(')[0];
};

const getArguments = bindingValue => {
  let args = bindingValue.split('(')[1].replace(')', '');
  if (args.includes("'")) {
    args = args.replace(/'/g, '');
  }
  return args.split(',').map(item => {
    return item.trim();
  });
};

const getBoundComments = (instance, variableName, value) => {
  return getBindStart(instance, variableName) + value + getBindEnd(instance, variableName);
};

const getBindStart = (_bindingCount, variableName) => {
  return `<!--flaap-target-${_bindingCount}-->`;
};

const getBindEnd = (_bindingCount, variableName) => {
  return `<!--flaap-target-${_bindingCount}/-->`;
};

const bindings = new Bindings();
export { bindings };
