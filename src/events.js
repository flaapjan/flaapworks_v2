/*
   __                 _     ___           
  /__\_   _____ _ __ | |_  / __\_   _ ___ 
 /_\ \ \ / / _ \ '_ \| __|/__\// | | / __|
//__  \ V /  __/ | | | |_/ \/  \ |_| \__ \
\__/   \_/ \___|_| |_|\__\_____/\__,_|___/
                                          
*/

let EventBus = {
  subscriptions: {},

  init: function(view) {
    return {
      view,
      publish: function(event, data) {
        console.log('%c ::>> Publishing Event ' + event, 'color:white;background:lightblue;');
        if (EventBus.subscriptions[event]) {
          for (let key in EventBus.subscriptions[event]) {
            if (EventBus.subscriptions[event].hasOwnProperty(key)) {
              EventBus.subscriptions[event][key](event, data);
            }
          }
        }
      },
      subscribe: function(event, callback) {
        console.log('%c ::>> Subscribing to Event ' + event + ' in ' + view, 'color:white;background:lightgreen;');
        EventBus.subscriptions[event] = EventBus.subscriptions[event] || {};
        EventBus.subscriptions[event][this.view] = EventBus.subscriptions[event][this.view] || callback;
      },
      subscribeOnce: function(event, callback) {
        this.subscribe(event, data => {
          this.unsubscribe(event);
          callback(data);
        });
      },
      unsubscribe: function(event) {
        if (EventBus.subscriptions[event] && EventBus.subscriptions[event][this.view]) {
          delete EventBus.subscriptions[event][this.view];

          if (!Object.keys(EventBus.subscriptions[event]).length) {
            delete EventBus.subscriptions[event];
          }
        }
        console.log('%c ::>> removed subscription ', 'color:white;background:red;');
        console.log(' ::>> EventBus.subscriptions = ', EventBus.subscriptions);
      }
    };
  }
};

export { EventBus };
