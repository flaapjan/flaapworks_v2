const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const BabiliPlugin = require('babili-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const appRoot = './test';

module.exports = {
  entry: [appRoot + '/index.js'],
  output: {
    library: 'flaap',
    libraryTarget: 'umd',
    path: path.join(__dirname, 'dist'),
    publicPath: '',
    filename: 'dist.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: { presets: ['env', 'stage-0'] }
        }
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: {
            loader: 'css-loader',
            options: {
              minimize: true,
              sourceMap: true
            }
          }
        })
      },
      {
        test: /\.html$/,
        use: ['html-loader']
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]'
          }
        }
      },
      {
        test: /\.png$/,
        use: {
          loader: 'file-loader',
          options: {
            name: 'public/images/[name].[ext]'
          }
        }
      }
    ]
  },
  plugins: [
    // new BabiliPlugin(),
    new ExtractTextPlugin('dist.css')
  ]
};
