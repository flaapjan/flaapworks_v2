/*
  ___ _                                     _            __                           _      
  / __\ | __ _  __ _ _ ____      _____  _ __| | _____    /__\_  ____ _ _ __ ___  _ __ | | ___ 
 / _\ | |/ _` |/ _` | '_ \ \ /\ / / _ \| '__| |/ / __|  /_\ \ \/ / _` | '_ ` _ \| '_ \| |/ _ \
/ /   | | (_| | (_| | |_) \ V  V / (_) | |  |   <\__ \ //__  >  < (_| | | | | | | |_) | |  __/
\/    |_|\__,_|\__,_| .__/ \_/\_/ \___/|_|  |_|\_\___/ \__/ /_/\_\__,_|_| |_| |_| .__/|_|\___|
                    |_|                                                         |_|           
*/

import { flaapworks as Flaapworks } from '../src/main';

let __flaapworks__ = (function configure(flaapworks) {
  return flaapworks.configure //
    .routes([
      { route: ['', 'one'], module: 'test/views/one/one', name: 'one' }, //
      { route: 'two', module: 'test/views/two/two', name: 'two' }
    ]);
})(Flaapworks);

window.routeTo = (state, test) => {
  console.log(' ::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> data >> ', { state, test });
  __flaapworks__.router.navigate(state);
};
