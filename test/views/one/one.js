class One {
  constructor() {
    console.log(' ::>> loading state one ', this);
    // instantiate initial values like so:
    this.name = 'Flaap';
    this.test = 'test value';

    this.timeout = setTimeout(() => {
      // update values like so:
      this.name.set('Tiffany');

      this.timeout = setTimeout(() => {
        this.name.set('Jeff');

        this.timeout = setTimeout(() => {
          this.test.set('hello');
        }, 3000);

        console.log(' ::>> current name = ', this.name.get());
      }, 3000);
    }, 3000);
  }

  activate() {
    // todo: add activation logic here
    console.log(' ::>> state one >> activated >> ');
  }

  attached() {
    // todo: add attached logic here
    console.log(' ::>> state one >> attached >> ');
  }

  deactivate() {
    // todo: add deactivate logic here
    console.log(' ::>> state one >> deactivate >> ');

    if (this.timeout) {
      window.clearTimeout(this.timeout);
    }
    // this.name.set(undefined);
    // this.test.set(undefined);
  }
}
