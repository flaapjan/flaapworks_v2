class Two {
  constructor() {
    console.log(' ::>> loading state two ', this);
  }

  activate() {
    // todo: add activation logic here
    console.log(' ::>> state two >> activated >> ');
  }

  attached() {
    // todo: add attached logic here
    console.log(' ::>> state two >> attached >> ');
  }

  deactivate() {
    // todo: add deactivate logic here
    console.log(' ::>> state two >> deactivate >> ');
  }
}
